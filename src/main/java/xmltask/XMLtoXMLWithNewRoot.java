package xmltask;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XMLtoXMLWithNewRoot {
    public static void main(String[] args) throws IOException {
        ConsoleEvents console = new ConsoleEvents();
        File analisedXML = console.chooseFileMenu();
        System.out.println(analisedXML);
        convertXMLtoXML(analisedXML);
    }
    public static void convertXMLtoXML (File xml){
        Source sourceXML = new StreamSource(xml);
        String path = xml.getPath();
        //String resultPath = path.substring(0,path.length()-4)+".xsl";
        //System.out.println(resultPath);
        Source xslt = new StreamSource("src\\xmls\\changeroot.xsl");
        String newXMLPath = path.substring(0,path.length()-4)+"newroot.xsl";

        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(newXMLPath);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);
            transform.transform(sourceXML,new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            System.out.println(" newRoot.xml file generated succesfully at " + newXMLPath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
