package xmltask;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleEvents {


    public File chooseFileMenu() throws IOException {
//        System.out.println("Sentence  |   File");
        System.out.println("Choose a file to parse: ") ;
        File [] files = getFiles();
        String text;
        for (int i = 1; i<= files.length;i++){
            System.out.println(i + " --- " + files [i-1].getName());
        }
        int fileNumber = 0;
        do {
            System.out.println("Choose filse from list(tipe number)" );
            try {
                Scanner input = new Scanner(System.in);
                fileNumber = input.nextInt();
                System.out.println(
                        "The number entered is " + fileNumber);
            } catch(InputMismatchException ex){
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                chooseFileMenu();
            }
        } while (0 > fileNumber && fileNumber >= files.length);

        File file = Paths.get(files[fileNumber-1].getPath()).toFile();

        return file;
    }

    private File [] getFiles() {
        File folder = new File("src\\xmls");
        return folder.listFiles();
    }

    public String chooseParcerMenu() {
        Scanner input = new Scanner(System.in);
        String answer;
        do {
            System.out.println("Choose method of parsing");
            System.out.println("1------DOM");
            System.out.println("2------SAX");
            System.out.println("3------StAX");
            answer = input.nextLine();
        }
        while (!answer.equalsIgnoreCase("1") && !answer.equalsIgnoreCase("2")
                && !answer.equalsIgnoreCase("2"));
        return answer;
    }
}
