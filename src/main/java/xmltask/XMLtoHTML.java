package xmltask;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class XMLtoHTML {
    public static void main(String[] args) throws IOException {
        ConsoleEvents console = new ConsoleEvents();
        File analisedXML = console.chooseFileMenu();
        System.out.println(analisedXML);
        convertXMLtoHTML(analisedXML);
    }
    public static void convertXMLtoHTML (File xml){
        Source sourceXML = new StreamSource(xml);
        String path = xml.getPath();
        //String resultPath = path.substring(0,path.length()-4)+".xsl";
        //System.out.println(resultPath);
        Source xslt = new StreamSource("src\\xmls\\medicines2.xsl");
        String htmlPath = path.substring(0,path.length()-4)+".html";

        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(htmlPath);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);
            transform.transform(sourceXML,new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            System.out.println(" .html file generated succesfully at " + htmlPath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
