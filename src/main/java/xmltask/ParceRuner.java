package xmltask;

import model.Medicine;
import model.Medicines;
import org.xml.sax.SAXException;
import parsers.MedicineStAXParser;
import parsers.MedicinesDOMParser;
import parsers.MedicinesSAXParcer;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class ParceRuner {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        ConsoleEvents console = new ConsoleEvents();
        File analisedXML = console.chooseFileMenu();
        System.out.println(analisedXML);
        String ansver = console.chooseParcerMenu();
        Medicines medicines;
        if (ansver.equalsIgnoreCase("1"))medicines = MedicinesDOMParser.parceDome(analisedXML);
        else if (ansver.equalsIgnoreCase("2"))medicines = MedicinesSAXParcer.parseSAX(analisedXML);
        else medicines = MedicineStAXParser.staxPatser(analisedXML);

        for (Medicine med : medicines.getMedicines()) med.printMedicineInfo();
    }

}
