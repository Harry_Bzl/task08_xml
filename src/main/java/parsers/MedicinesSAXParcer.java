package parsers;

import model.Medicines;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class MedicinesSAXParcer {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    public static Medicines parseWithoutValidation (File xmlFile){
        Medicines medicines = new Medicines();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler saxHandler = new SaxHandler();
            saxParser.parse(xmlFile, saxHandler);
            medicines = saxHandler.getMedicines();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return medicines;
    }

    public static Medicines parseSAX (File xmlFile){
        File xsd= new File("src\\xmls\\medicines2.xsd");
        Medicines medicines = new Medicines();
        try {
            saxParserFactory.setSchema(DOMValidator.createSchema(xsd));
            saxParserFactory.setValidating(true);

            SAXParser saxParser = saxParserFactory.newSAXParser();
            System.out.println(saxParser.isValidating());
            SaxHandler saxHandler = new SaxHandler();
            saxParser.parse(xmlFile, saxHandler);
            medicines = saxHandler.getMedicines();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            ex.printStackTrace();
        }
        return medicines;
    }


}
