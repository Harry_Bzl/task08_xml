package parsers;

import model.Dosage;
import model.Group;
import model.Medicine;
import model.Medicines;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MedicinesDOMParser {
    public static Medicines parceDome (File file) throws ParserConfigurationException, IOException, SAXException {

        Medicines medicines = new Medicines();


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        Element root = doc.getDocumentElement();

        if (root.getTagName().equalsIgnoreCase("medicines")){
            NodeList nodeList = root.getChildNodes();
            for (int i=0; i<nodeList.getLength(); i++){
                Node child = nodeList.item(i);
                if (child instanceof Element){
                    Element childElement = (Element)child;
                    if (childElement.getTagName().equalsIgnoreCase("medicine")){
                        medicines.addMedicine(parseMedicine(childElement));
                    }
                }
            }
        }
        else System.out.println("XML document don't have root 'medicines'!!!");



        return medicines;
    }

    private static Medicine parseMedicine(Element element){
        Medicine medicine;
        String name = "no name";
        String pharm = "no pharm";
        Group grop = new Group("no name group", "no code");
        List <String> analogs = new ArrayList<>();
        List <String> versions = new ArrayList<>();
        String certificate = "no certificate";
        List<Integer> packages = new ArrayList<>();
        Dosage dosage = new Dosage("no dosage unit", -1);

        NodeList nodeList = element.getChildNodes();
        for (int i=0; i<nodeList.getLength(); i++){
            Node child = nodeList.item(i);
            if (child instanceof Element){
                Element childElement = (Element)child;
                if (childElement.getTagName().equalsIgnoreCase("name")){
                    name = getNodeInfo(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("pharm")){
                    pharm = getNodeInfo(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("group")){
                    String groupName = getNodeInfo(childElement);
                    String groupCode = childElement.getAttribute("code");
                    grop = new Group(groupName,groupCode);
                }
                else if (childElement.getTagName().equalsIgnoreCase("analogs")){
                    analogs = parseStringList(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("versions")){
                    versions = parseStringList(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("certificate")) {
                    certificate = getNodeInfo(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("packages")){
                    packages = parseIntegerList(childElement);
                }
                else if (childElement.getTagName().equalsIgnoreCase("dosage")){
                    Integer value = Integer.parseInt(getNodeInfo(childElement));
                    String unit = childElement.getAttribute("unit");
                    dosage = new Dosage(unit, value);
                }

            }
        }
        medicine = new Medicine(name,pharm,grop);
        medicine.setCertificate(certificate);
        medicine.setDosage(dosage);
        medicine.setAnalogs(analogs);
        medicine.setPackages(packages);
        medicine.setVersions(versions);
        return  medicine;
    }

    private static String getNodeInfo(Element childElement) {
        String name;
        Text textNode = (Text)childElement.getFirstChild();
        name = textNode.getData().trim();
        //System.out.println(name);
        return name;
    }

    private static List<String> parseStringList(Element childElement) {
        List <String> values = new ArrayList<>();
        NodeList nodeList = childElement.getChildNodes();
        for (int i=0; i<nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            if (child instanceof Element) {
                Element childElem = (Element) child;
                String info = getNodeInfo(childElem);
                values.add(info);
            }
        }

        return values;
    }
    private static List<Integer> parseIntegerList(Element childElement) {
        List <Integer> values = new ArrayList<>();
        NodeList nodeList = childElement.getChildNodes();
        for (int i=0; i<nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            if (child instanceof Element) {
                Element childElem = (Element) child;
                String info = getNodeInfo(childElem);
                values.add(Integer.parseInt(info));
            }
        }

        return values;
    }
}
