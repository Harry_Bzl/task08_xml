package parsers;

import model.Dosage;
import model.Group;
import model.Medicine;
import model.Medicines;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MedicineStAXParser {
    public static Medicines staxPatser(File file) {
        Medicines medicines = new Medicines();
        Medicine medicine;
        String name = "no name";
        String pharm = "no pharm";
        Group group = new Group("no name group", "no code");
        List<String> analogs = new ArrayList<>();
        List <String> versions = new ArrayList<>();
        String certificate = "no certificate";
        List<Integer> packages = new ArrayList<>();
        Dosage dosage = new Dosage("no dosage unit", -1);

        XMLInputFactory factory = XMLInputFactory.newInstance();
        try (FileInputStream reader = new FileInputStream(file)) {

            XMLEventReader xmlEventReader = factory.createXMLEventReader(reader);
            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("name")){
                        xmlEvent = xmlEventReader.nextEvent();
                        name = xmlEvent.asCharacters().getData();
                        //System.out.println(name);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("pharm")){
                        xmlEvent = xmlEventReader.nextEvent();
                        pharm = xmlEvent.asCharacters().getData();
                        //System.out.println(pharm);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("group")){
                        xmlEvent = xmlEventReader.nextEvent();
                        String groupName = xmlEvent.asCharacters().getData();
                        //System.out.println(groupName);
                        Attribute idAttr = startElement.getAttributeByName(new QName("code"));
                        if(idAttr != null){
                            group = new Group(groupName,idAttr.getValue());
                        }
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("analogs")){
                        //xmlEvent = xmlEventReader.nextEvent();
                        analogs = new ArrayList<>();
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("analog")){
                        xmlEvent = xmlEventReader.nextEvent();
                        String analog = xmlEvent.asCharacters().getData();
                        analogs.add(analog);
                        //System.out.println(analog);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("versions")){
                        //xmlEvent = xmlEventReader.nextEvent();
                        versions = new ArrayList<>();
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("version")){
                        xmlEvent = xmlEventReader.nextEvent();
                        String version = xmlEvent.asCharacters().getData();
                        versions.add(version);
                        //System.out.println(version);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("certificate")){
                        xmlEvent = xmlEventReader.nextEvent();
                        certificate = xmlEvent.asCharacters().getData();
                        //System.out.println(certificate);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("packages")){
                        //xmlEvent = xmlEventReader.nextEvent();
                        packages = new ArrayList<>();
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("package")){
                        xmlEvent = xmlEventReader.nextEvent();
                        Integer pack = Integer.parseInt(xmlEvent.asCharacters().getData());
                        packages.add(pack);
                        //System.out.println(pack);
                    }
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("dosage")){
                        xmlEvent = xmlEventReader.nextEvent();
                        String dose = xmlEvent.asCharacters().getData();
                        //System.out.println(dose);
                        Attribute idAttr = startElement.getAttributeByName(new QName("unit"));
                        if(idAttr != null){
                            dosage = new Dosage(idAttr.getValue(),Integer.parseInt(dose));
                        }
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equalsIgnoreCase("medicine")) {
                        medicine = new Medicine(name,pharm,group);
                        medicine.setCertificate(certificate);
                        medicine.setDosage(dosage);
                        medicine.setAnalogs(analogs);
                        medicine.setPackages(packages);
                        medicine.setVersions(versions);
                        medicines.addMedicine(medicine);

                        name = "no name";
                        pharm = "no pharm";
                        group = new Group("no name group", "no code");
                        analogs = new ArrayList<>();
                        versions = new ArrayList<>();
                        certificate = "no certificate";
                        packages = new ArrayList<>();
                        dosage = new Dosage("no dosage unit", -1);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return medicines;
    }
}
