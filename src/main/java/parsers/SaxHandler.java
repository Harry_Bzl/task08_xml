package parsers;


import model.Dosage;
import model.Group;
import model.Medicine;
import model.Medicines;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SaxHandler extends DefaultHandler {

    private Medicines medicines = new Medicines();
    private String name = "no name";
    private String pharm = "no pharm";
    private String gropName = "No groupName";
    private String code = "no code";
    private Group group = new Group(gropName, code);
    private List<String> analogs = new ArrayList<>();
    private List <String> versions = new ArrayList<>();
    private String certificate = "no certificate";
    private List<Integer> packages = new ArrayList<>();
    private String dosageUnit = "no dosage unit";
    private Integer dose = -1;
    private Dosage dosage = new Dosage(dosageUnit, dose);
    private String currentElement;


    public Medicines getMedicines() {
        return this.medicines;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = qName;

        if (currentElement.equalsIgnoreCase("group")){
            code = attributes.getValue("code");
        }
        else if (currentElement.equalsIgnoreCase("dosage"))
            dosageUnit = attributes.getValue("unit");
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        currentElement = qName;
        if (currentElement.equalsIgnoreCase("medicine")) {
            Medicine med = createMedicine();
            med.printMedicineInfo();
            medicines.addMedicine(med);
            this.setClassInstancesToDefalt();
        }
        else if (currentElement.equalsIgnoreCase("group")){
            System.out.println("New group");
            group = new Group(gropName,code);
        }
        else if (currentElement.equalsIgnoreCase("dosage")){
            dosage = new Dosage(dosageUnit,dose);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (currentElement.equals("name")) {
            name = new String(ch, start, length);
        }
        else if (currentElement.equals("group")) {
            gropName = new String(ch, start, length);
        }
        if (currentElement.equals("analog")) {
            analogs.add(new String(ch, start, length));
        }
        if (currentElement.equals("version")) {
            versions.add(new String(ch, start, length));
        }
        else if (currentElement.equals("certificate")) {
            certificate = new String(ch, start, length);
        }
        if (currentElement.equals("package")) {
            packages.add(Integer.parseInt(new String(ch, start, length)));
        }
        else if (currentElement.equals("dosage")) {
            dose = Integer.parseInt(new String(ch, start, length));
        }
    }

    private Medicine createMedicine (){
        Medicine currentMedicine;
        currentMedicine = new Medicine(name,pharm,group);
        currentMedicine.setCertificate(certificate);
        currentMedicine.setDosage(dosage);
        currentMedicine.setAnalogs(analogs);
        currentMedicine.setPackages(packages);
        currentMedicine.setVersions(versions);
        return currentMedicine;
    }

    private void setClassInstancesToDefalt (){

        name = "no name";
        pharm = "no pharm";
        gropName = "No groupName";
        code = "no code";
        group = new Group(gropName, code);
        analogs = new ArrayList<>();
        versions = new ArrayList<>();
        certificate = "no certificate";
        packages = new ArrayList<>();
        dosageUnit = "no dosage unit";
        dose = -1;
        dosage = new Dosage(dosageUnit, dose);
    }

    private boolean checkEmpty (String str){
        String regex = "\\w*";
        Pattern pattern= Pattern.compile(regex);
        Matcher m = pattern.matcher(str);
        if (m.find())return true;
        return false;
    }
}