package model;

public class Dosage {
    String unit;
    Integer dose;

    public Dosage(String unit, Integer dose) {
        this.unit = unit;
        this.dose = dose;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getDose() {
        return dose;
    }
}
