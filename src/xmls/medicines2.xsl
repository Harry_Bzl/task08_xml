<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #2b2b2b">
                <div style="background-color: green; color: black; text-align:center;">
                    <h1> Medicines </h1>
                </div>
                <table border="3">
                    <tr bgcolor="#86c4cc">
                        <th >Name</th>
                        <th>Manufacturer:</th>
                        <th>Pharmacological group, code</th>
                        <th>Analogs</th>
                        <th>Versions</th>
                        <th>Certificate</th>
                        <th>Packages</th>
                        <th>Dosage, unit</th>
                    </tr>
                    <xsl:for-each select="medicines/medicine">
                        <xsl:sort select="name"/>
                        <tr bgcolor="#e6ffe6">
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="pharm"/></td>
                            <td>
                                <xsl:for-each select="group">
                                    <xsl:value-of select="." />
                                    <xsl:text>, </xsl:text>
                                    <xsl:value-of select="@code"/>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:for-each select="analogs/analog">
                                    <xsl:value-of select="." />
                                    <xsl:text>, </xsl:text>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:for-each select="versions/version">
                                    <xsl:value-of select="."/>
                                    <xsl:text>, </xsl:text>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:value-of select="certificate"/>
                            </td>
                            <td>
                                <xsl:for-each select="packages/package">
                                    <xsl:value-of select="."/>
                                    <xsl:text>, </xsl:text>
                                </xsl:for-each>
                            </td>
                            <td>
                                <xsl:for-each select="dosage">
                                    <xsl:value-of select="." />
                                    <xsl:value-of select="@unit"/>
                                </xsl:for-each>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>