<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="medicines">
        <newroot>
            <xsl:apply-templates select="@* | node()" />
        </newroot>
    </xsl:template>
</xsl:stylesheet>