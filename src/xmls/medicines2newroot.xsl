<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="medicines2.xsl"?>
<newroot xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="medicines2.xsd">
    <medicine>
        <name>Nimesil</name>
        <pharm>Laboratori GUIDOTTI S.p.A.</pharm>
        <group code="M01A X17">Nonselective non-steroidal anti-inflammatory drugs</group>
        <analogs>
            <analog>Nimid</analog>
            <analog>Nimesin</analog>
            <analog>Nimesulid</analog>
        </analogs>
        <versions>
            <version>granules</version>
        </versions>
        <certificate>Approved by the Ministry of Health of Ukraine from 05.05.2015 № 323. Rp UA / 9855/01/01</certificate>
        <packages>
            <package>9</package>
            <package>15</package>
            <package>30</package>
        </packages>
        <dosage unit="g">2</dosage>
    </medicine>
    <medicine>
        <name>No-Shpa</name>
        <pharm>Sanofi</pharm>
        <group code="A03A D02">Means used in functional gastrointestinal disorders</group>
        <analogs>
            <analog>Drotaterin</analog>
            <analog>Nispasm Forte</analog>
        </analogs>
        <versions>
            <version>tablets</version>
        </versions>
        <certificate>Approved by the Ministry of Health of Ukraine from 01/27/2017 № 887. Rp UA / 0391/01/02</certificate>
        <packages>
            <package>10</package>
            <package>12</package>
            <package>20</package>
            <package>60</package>
            <package>24</package>
        </packages>
        <dosage unit="mg">40</dosage>
    </medicine>
    <medicine>
        <name>Corvitol</name>
        <pharm>Berlin-Chemie AG</pharm>
        <group code="C07A B02">Selective beta-adrenoceptor blockers</group>
        <analogs>
            <analog>Metoprolol</analog>
            <analog>Betalok</analog>
            <analog>Egilok</analog>
        </analogs>
        <versions>
            <version>tablets</version>
        </versions>
        <certificate>Approved by the Ministry of Health of Ukraine from 01/27/2017 № 887. Rp UA / 0391/01/02</certificate>
        <packages>
            <package>10</package>
            <package>30</package>
            <package>50</package>
            <package>100</package>
            <package>24</package>
        </packages>
        <dosage unit="mg">50</dosage>
    </medicine>
</newroot>